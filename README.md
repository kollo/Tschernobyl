# Tschernobyl
<img alt="Logo" src="fastlane/metadata/android/en-US/images/icon.png" width="120" />

Tschernobyl für Android
=======================

Copyright (c) 1987-2021 Markus Hoffmann

Programm zur Abschätzung der Folgen durch radioaktive 
Belastung nach einem Kernkraftwerksunfall. Ausgangsgröße ist die gemessene 
radioaktive Belastung der Luft, wie sie beim Erreichen des betroffenen Gebietes 
zuerst gemessen wird.  Daraus werden  die Strahlenbelastungen durch die 
Direktstrahlung, Bodenstrahlung und Nahrungsaufnahme abgeschätzt.  Dann werden die Risiken analysiert und Verhaltensmaßregeln abgeleitet.


Tschernobyl and Consequences
============================

Copyright (c) 1987-2021 Markus Hoffmann

Program to estimate the consequences of radioactive  pollution after a nuclear
power plant accident. The starting point is the measured radioactive
contamination of the air, as measured  when it reaches the affected area first.
From this, the radiation exposure from  direct radiation, soil radiation and
food intake is estimated. Then the risks are  analyzed and behavioral measures
are derived.

### Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/en/packages/de.drhoffmannsoftware/)


### Screenshots

<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="25%">
<img alt="App image" src="fastlane/metadata/android/de-DE/images/phoneScreenshots/2.png" width="25%">
<img alt="App image" src="fastlane/metadata/android/de-DE/images/phoneScreenshots/3.png" width="25%">
<img alt="App image" src="fastlane/metadata/android/de-DE/images/phoneScreenshots/4.jpg" width="25%">
</div>

### Important Note:

The WRITE EXTERNAL STORAGE permission is needed to write the reports to the file system, 

No software can be perfect. We do our best to keep this app bug free, 
improve it and fix all known errors as quick as possible. 
However, this program is distributed in the hope that it will 
be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Use this program on your own risk.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; Version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.

 git clone git@codeberg.org:kollo/Tschernobyl.git

then do a 
  cd Tschernobyl
  ./gradlew build
(Enter passwords for the keystore)
(the compile process will take a while.)

The apk should finally be in build/outputs/apk/Tschernobyl-release.apk

