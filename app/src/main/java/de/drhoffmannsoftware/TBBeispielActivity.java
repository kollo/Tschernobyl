package de.drhoffmannsoftware;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Button;


public class TBBeispielActivity extends MyActivity {
  private static int mid;
  private static String titel;
  private TextView btext1,btext1b,btext2,btext3;
  private Button weiterbutton;
  private Button fertig;
  private EditText eingabe,eingabe2;
  private RadioGroup radio;
  private RadioButton radio0,radio1;
  private ScrollView scrollview;
	
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.beispiel1);
    weiterbutton=findViewById(R.id.weiterbutton);
    fertig=findViewById(R.id.fertig);
    Button next = findViewById(R.id.next);
    weiterbutton.setEnabled(false);
        
    eingabe= findViewById(R.id.editText1);
    eingabe2= findViewById(R.id.editText2);
    radio=findViewById(R.id.radioGroup1);
    radio0=findViewById(R.id.radio0);
    radio1=findViewById(R.id.radio1);
    TextView btitel = findViewById(R.id.btitel);
    WebView webview = findViewById(R.id.webView1);
    scrollview=findViewById(R.id.scroll);
    btitel.setText(getResources().getString(R.string.word_beispiel)+" "+titel); 
    btext1=findViewById(R.id.btext1);
    btext1b=findViewById(R.id.btext1b);
    btext2=findViewById(R.id.btext2);
    btext3=findViewById(R.id.btext3);
    btext2.setVisibility(View.INVISIBLE);
    btext3.setVisibility(View.INVISIBLE);
    String[] text1 = getResources().getStringArray(R.array.beispieltext1);
    btext1.setText(Html.fromHtml(text1[mid]));
    next.setVisibility(View.GONE);
    webview.setVisibility(View.GONE);
    fertig.setEnabled(false);
        
    if(mid==0) btext1b.setText(getResources().getString(R.string.fragment_1));
    else if(mid==1) btext1b.setText(getResources().getString(R.string.fragment_2));
    else if(mid==2) btext1b.setText(getResources().getString(R.string.fragment_3));
    else if(mid==5) {
      btext1b.setText(getResources().getString(R.string.fragment_4));
      radio.setVisibility(View.GONE);
    } else {
      btext1b.setText("");
      eingabe2.setVisibility(View.GONE);
      radio.setVisibility(View.GONE);
    }
    if(mid==3 || mid==4) {
      btext1b.setVisibility(View.GONE);
    }
        
    eingabe.addTextChangedListener(new TextWatcher(){
      public void onTextChanged(CharSequence s, int start, int before, int count) {}
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      public void afterTextChanged(Editable s) {
        weiterbutton.setEnabled(s.length()>0);
      }
    });
    weiterbutton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        weiterbutton.setEnabled(false);
        eingabe.setEnabled(false);
        eingabe2.setEnabled(false);
        double wert=Double.parseDouble(eingabe.getText().toString());
        if(mid==0) {
          boolean ke=radio0.isChecked();
          double liter=wert;
          String t;
          radio0.setEnabled(false);
	      radio1.setEnabled(false);
	      radio.setVisibility(View.GONE);
          eingabe.setVisibility(View.GONE);
	      eingabe2.setVisibility(View.GONE);
	      btext1b.setVisibility(View.GONE);
          double belast=Double.parseDouble(eingabe2.getText().toString());
          if(ke) btext1.setText(String.format(getResources().getString(R.string.fragment_5),displaymenge(liter),displayactivity(belast)));
          else   btext1.setText(String.format(getResources().getString(R.string.fragment_6),displaymenge(liter),displayactivity(belast)));
          double gi=liter*belast;
          btext2.setText(String.format(getResources().getString(R.string.fragment_7),displaymenge(gi)));
          if(ke) {
            double chdos=gi*0.043;
            double effdos=gi*0.0013;
            t=String.format(getResources().getString(R.string.fragment_8),displaydosis(chdos));
            t=t+" "+String.format(getResources().getString(R.string.fragment_9),displaydosis(effdos));
          } else {
            double chdos=gi*0.35;
            double effdos=gi*0.01;
            t=String.format(getResources().getString(R.string.fragment_10),displaydosis(chdos));
            t=t+" "+String.format(getResources().getString(R.string.fragment_9),displaydosis(effdos));
          } 
          t=t+"<p/>"+String.format(getResources().getString(R.string.fragment_11),displaydosis(110),displaydosis(5000));
          btext3.setText(Html.fromHtml(t));
        } else if(mid==1) {
          boolean ke=radio0.isChecked();
          String t;
          radio0.setEnabled(false);
	      radio1.setEnabled(false);
	      radio.setVisibility(View.GONE);
          eingabe.setVisibility(View.GONE);
	      eingabe2.setVisibility(View.GONE);
	      btext1b.setVisibility(View.GONE);
          double belast=Double.parseDouble(eingabe2.getText().toString());
          double gi=wert*belast;
          if(ke) btext1.setText(String.format(getResources().getString(R.string.fragment_12),displaymenge(wert),displayactivity(belast)));
          else   btext1.setText(String.format(getResources().getString(R.string.fragment_13),displaymenge(wert),displayactivity(belast)));
          btext2.setText(String.format(getResources().getString(R.string.fragment_7),displaymenge(gi)));
          if(ke) {
            double aados=gi*1.4/10000.0;  /*Fehler?? im Orginal faktor 10 groesser ??*/
            t=String.format(getResources().getString(R.string.fragment_14),displaydosis(aados));
          } else {
            double aados=gi*9.3/10000.0; 
            t=String.format(getResources().getString(R.string.fragment_15),displaydosis(aados));
          } 
          btext3.setText(Html.fromHtml(t));
        } else if(mid==2) {
          boolean ke=radio0.isChecked();
          String t;
          radio0.setEnabled(false);
	  radio1.setEnabled(false);
	  radio.setVisibility(View.GONE);
          eingabe.setVisibility(View.GONE);
	  eingabe2.setVisibility(View.GONE);
	  btext1b.setVisibility(View.GONE);
          double belast=Double.parseDouble(eingabe2.getText().toString());
          double gi=wert*belast;
          if(ke) btext1.setText(String.format(getResources().getString(R.string.fragment_12),displaymenge(wert),displayactivity(belast)));
          else   btext1.setText(String.format(getResources().getString(R.string.fragment_13),displaymenge(wert),displayactivity(belast)));
          btext2.setText(String.format(getResources().getString(R.string.fragment_7),displaymenge(gi)));
      	  if(ke) {
            double kmdos=gi*0.011;
            double gdos=gi*0.0035;
            t=String.format(getResources().getString(R.string.fragment_20),displaydosis(kmdos));
            t=t+" "+String.format(getResources().getString(R.string.fragment_9),displaydosis(gdos));
          } else {
            double kmdos=gi*0.1;
            double gdos=gi*0.011;
            t=String.format(getResources().getString(R.string.fragment_21),displaydosis(kmdos));
            t=t+" "+String.format(getResources().getString(R.string.fragment_9),displaydosis(gdos));
          } 
          btext3.setText(Html.fromHtml(t));
        } else if(mid==3) {
          weiterbutton.setVisibility(View.GONE);
          if(wert<3.0) btext2.setText(getResources().getString(R.string.fragment_17));
          else if(wert<10.0) btext2.setText(getResources().getString(R.string.fragment_18));
          else btext2.setText(getResources().getString(R.string.fragment_19));
          btext3.setText(String.format(getResources().getString(R.string.fragment_22),displaydosis(9)));
        } else if(mid==4){
          weiterbutton.setVisibility(View.GONE);
          if(wert>2.0 && wert<20.0) btext2.setText(getResources().getString(R.string.fragment_16));
          else btext2.setText("");
          btext3.setText(String.format(getResources().getString(R.string.fragment_23),displaydosis(7)));
      } else if(mid==5) {
        double fludos,boddos;
        double fluz=wert;
        double geog=Double.parseDouble(eingabe2.getText().toString());
        if(geog>50)  geog=50;
        fludos=fluz/8760.0*(1000.0+40.0*java.lang.Math.pow(geog/10.0,2.5));
        boddos=fluz/8760.0*120.0;
        eingabe.setVisibility(View.GONE);
        eingabe2.setVisibility(View.GONE);
        btext1b.setVisibility(View.GONE);
        btext1.setText(String.format(getResources().getString(R.string.fragment_24),displaymenge(fluz),displaymenge(geog)));
        btext2.setText(String.format(getResources().getString(R.string.fragment_25),displaydosis(fludos)));
        btext3.setText(String.format(getResources().getString(R.string.fragment_26),displaydosis(boddos)));
      } else btext2.setText("Test: "+mid+" "+wert);
        btext2.setVisibility(View.VISIBLE);
        btext3.setVisibility(View.VISIBLE);
        scrollview.smoothScrollTo(0, scrollview.getHeight());
        fertig.setEnabled(true);
      }
    });
    fertig.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        finish();
      }
    });
  }
  public static void setmenu(int i) {
    mid=i;
  }
  public static void settitle(String t) {
    titel=t;
  }
}
