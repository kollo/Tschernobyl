package de.drhoffmannsoftware;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Button;

public class TBPageActivity extends Activity {
	private static int mid=0;
    private TextView page;
    private TextView text;
    private int i;
    private static String[] seiten;
    private static String titel;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page);
        TextView name = findViewById(R.id.name);
        
        if(mid==1) seiten = getResources().getStringArray(R.array.literatur);
        else if(mid==2) { 
        	/* Seiten sind dann schon gesetzt! */
        	name.setText(titel);
        } else seiten = getResources().getStringArray(R.array.manual);
        Button weiter = findViewById(R.id.forward);
        Button zurueck = findViewById(R.id.back);
        page=findViewById(R.id.pagenr);
        text=findViewById(R.id.text);
        i=0;
        page.setText(getString(R.string.pageind)+" "+(i+1)+"/"+seiten.length);
        text.setText(Html.fromHtml(seiten[i]));
/*        CharSequence ctext = text.getText();
        ctext = setSpanBetweenTokens(ctext, "##", new ForegroundColorSpan(0xFFFF0000));
        ctext = setSpanBetweenTokens(ctext, "$$", new StyleSpan(Typeface.BOLD_ITALIC));
        ctext = setSpanBetweenTokens(ctext, "==", new RelativeSizeSpan(1.2f), new ForegroundColorSpan(0xFFFFFFFF));
        
        text.setText(ctext);
  */      
        weiter.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		i++;
        		if(i>=seiten.length) finish();
        		else {
        			text.setText(Html.fromHtml(seiten[i]));
/*        			CharSequence ctext = text.getText();
        	        ctext = setSpanBetweenTokens(ctext, "##", new ForegroundColorSpan(0xFFFF0000));
        	        ctext = setSpanBetweenTokens(ctext, "$$", new StyleSpan(Typeface.BOLD_ITALIC));
        	        ctext = setSpanBetweenTokens(ctext, "==", new RelativeSizeSpan(1.2f), new ForegroundColorSpan(0xFFFFFFFF));
        	        
        	        text.setText(ctext); */
        			page.setText(getString(R.string.pageind)+" "+(i+1)+"/"+seiten.length);
        		}
        		}
        });
        zurueck.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		i--;
        		if(i<0) finish();
        		else { 
        			text.setText(Html.fromHtml(seiten[i]));
        			page.setText(getString(R.string.pageind)+" "+(i+1)+"/"+seiten.length);
        		}
        	}
        });
		
    }

	public static void setmenu(int i) {
		mid=i;
    }
	public static void setseiten(String[] t) {
		seiten=t;
    }
	public static void settitle(String t) {
		titel=t;
    }
}
