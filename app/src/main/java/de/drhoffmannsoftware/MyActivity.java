package de.drhoffmannsoftware;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;


/*Diese Klasse fuegt nur ein gemeinsames Menu hinzu. Kann dann von allen
 * Activities benutzt werden.*/


public class MyActivity extends Activity {
public static String munit="Sv";
@Override
protected Dialog onCreateDialog(final int id) {
	Dialog dialog = null;
	if(id==8) {
		dialog = new Dialog(MyActivity.this);
		final TextView wV = new TextView(this);
		String t;
		t=getResources().getString(R.string.feature_disabled);  
		wV.setText(Html.fromHtml(t));
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(wV);
		dialog.setCanceledOnTouchOutside(true);
  	}
	return dialog;
}	
	
	@Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.menu, menu);
	        return true;
	    }
	@Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	    	Intent viewIntent;
	        switch (item.getItemId()) {
	            case R.id.options_about:
	            	viewIntent = new Intent(MyActivity.this, TBInfoActivity.class);
	            	startActivity(viewIntent);
	            	break;
	            case R.id.options_settings:
	            	
	            	startActivity(new Intent(this, PreferencesActivity.class));
	            	
	            	break;
	           case R.id.options_help:
	            	TBPageActivity.setmenu(0);     
	            	viewIntent = new Intent(MyActivity.this, TBPageActivity.class);
	            	startActivity(viewIntent);
	            	break;
	           case R.id.options_finish:
	        	   finish();
	        	   break;
	            default: 
	            	return super.onOptionsItemSelected(item);
	        }
	        return true;
	    }
	public static double roundSignificant(double num, int n) {
	        if(num == 0) return 0;

	        final double d = Math.ceil(Math.log10(num < 0 ? -num: num));
	        final int p = n - (int) d;

	        final double m = Math.pow(10, p);
	        final long s = Math.round(num*m);
	        return s/m;
	    }
	public static String displaydosis(double mrem) {
        String t;
        Log.d("TAG","unit="+munit);
		if(munit.equals("Sv")) {
			t=roundSignificant(mrem*10.0,2)+" µSv";
		} else {
			t=roundSignificant(mrem,2)+" Millirem";
		}
		Log.d("TAG",t);	
		return t;
	}
	public static String displaydosis_nounit(double mrem) {
        String t;
		if(munit.equals("Sv")) {
			t=""+roundSignificant(mrem*10.0,2);
		} else {
			t=""+roundSignificant(mrem,2);
		}
		return t;
	}
	public static String displaydosis_flexible(double mrem) {
        String t;
        Log.d("TAG","unit="+munit);
		if(munit.equals("Sv")) {
			double a=roundSignificant(mrem*10.0,2);
			if(a>=10000000) t=(int)(a/1000000)+" Sv";
			else if(a>=1000000) t=a/1000000+" Sv";
			else if(a>=10000) t=(int)(a/1000)+" mSv";
			else if(a>=1000) t=a/1000+" mSv";
			else if(a>=10.0) t=(int)a+" µSv";
			else t=a+" µSv";
		} else {
			if(mrem>=10000) t=(int)(roundSignificant(mrem,2)/1000)+" Rem";
			else if(mrem>=1000) t=roundSignificant(mrem,2)/1000+" Rem";
			else if(mrem>=10) t=(int)roundSignificant(mrem,2)+" Millirem";
			else t=roundSignificant(mrem,2)+" Millirem";
		}
		Log.d("TAG",t);	
		return t;
	}
	public static String displayactivity(double bq) {
        String t;
			t=roundSignificant(bq,3)+" Bq";
		return t;
	}
	public static String displaymenge(double bq) {
        String t;
			t=roundSignificant(bq,3)+"";
		return t;
	}
}
