package de.drhoffmannsoftware;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/* Hauptprogramm. Implementiert eine Auswahlliste. Beim Starten wird gleich die
 * Intro-Activity gestartet. */

public class TschernobylActivity extends MyActivity {
  private String[] hauptmenutitel;
	static int cflag=0;
  public static int progfinished;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Intent myIntent = new Intent(TschernobylActivity.this, TBIntroActivity.class);
		if(cflag==0) startActivity(myIntent); 
        cflag=1;
       
		progfinished=0;
        
        hauptmenutitel = getResources().getStringArray(R.array.hauptmenutitel);

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        
        MyActivity.munit=prefs.getString("select_unit", "Sv");

		ListView liste = findViewById(R.id.hauptmenu);
        liste.setTextFilterEnabled(true);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, hauptmenutitel);
		liste.setAdapter(adapter);
		liste.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view,
        	        int position, long id) {
        		if(id==0) { 
        			
        			Intent myIntent = new Intent(TschernobylActivity.this, TBInfoActivity.class);
        			startActivity(myIntent); 
        		} else if(id==hauptmenutitel.length-1) {
        			finish();
        		} else if(id>=1 & id<hauptmenutitel.length) {

        			TBSubmenuActivity.setmenu((int)id);
        			TBSubmenuActivity.settitle(hauptmenutitel[(int)id]);

        			Intent myIntent = new Intent(TschernobylActivity.this, TBSubmenuActivity.class);
        			startActivity(myIntent); 
        		} 
        	}
        });
    }
}
