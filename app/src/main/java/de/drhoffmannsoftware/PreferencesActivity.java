package de.drhoffmannsoftware;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;

public class PreferencesActivity extends PreferenceActivity  implements OnSharedPreferenceChangeListener{
	private final static String HOMEPAGE="https://codeberg.org/kollo/Tschernobyl";
	private final static String LICENSE_PAGE="https://codeberg.org/kollo/Tschernobyl/raw/branch/master/LICENSE";
	private final static String SOURCE_PAGE="https://codeberg.org/kollo/Tschernobyl";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
         //   PreferenceManager.setDefaultValues(Preferences.this, R.xml.preferences, false);

            findPreference("about_version").setSummary(applicationVersion());
	    findPreference("about_homepage").setSummary(HOMEPAGE);
            
        //    findPreference("about_license").setSummary(Constants.LICENSE_URL);
        //    findPreference("about_source").setSummary(Constants.SOURCE_URL);
       //     findPreference("about_market_app").setSummary(String.format("market://details?id=%s", getPackageName()));
       //     findPreference("about_market_publisher").setSummary("market://search?q=pub:\"Markus Hoffmann\"");
            // Get a reference to the preferences
      //      mCheckBoxPreference = (CheckBoxPreference)getPreferenceScreen().findPreference(KEY_ADVANCED_CHECKBOX_PREFERENCE);
       //     mListPreference = (ListPreference)getPreferenceScreen().findPreference("select_fileformat");
            for(int i=0;i<getPreferenceScreen().getPreferenceCount();i++){
                initSummary(getPreferenceScreen().getPreference(i));
               }
	
	}
	 @Override
	    protected void onResume() {
	        super.onResume();
	        // Set up a listener whenever a key changes             
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
   
	    }

	    @Override
	    protected void onPause() {
	        super.onPause();
	        // Unregister the listener whenever a key changes             
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);     
 
	    }
	    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) { 
            updatePrefSummary(findPreference(key));
            MyActivity.munit=sharedPreferences.getString("select_unit", "Sv");
            
        }

	    private void initSummary(Preference p){
	           if (p instanceof PreferenceCategory){
	                PreferenceCategory pCat = (PreferenceCategory)p;
	                for(int i=0;i<pCat.getPreferenceCount();i++){
	                    initSummary(pCat.getPreference(i));
	                }
	            }else{
	                updatePrefSummary(p);
	            }

	        }

	        private void updatePrefSummary(Preference p){
	            if (p instanceof ListPreference) {
	                ListPreference listPref = (ListPreference) p; 
	                p.setSummary(listPref.getEntry()); 
	            }
	            if (p instanceof EditTextPreference) {
	                EditTextPreference editTextPref = (EditTextPreference) p; 
	                p.setSummary(editTextPref.getText()); 
	            }

	        }

    private String applicationVersion() {
            try {
                    return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            }
            catch (NameNotFoundException x)  {
                    return "unknown";
            }
    }

    @Override
    public boolean onPreferenceTreeClick(final PreferenceScreen preferenceScreen, final Preference preference)  {
            final String key = preference.getKey();
            if ("about_version".equals(key)) {
            	startActivity(new Intent(this, TBInfoActivity.class));
         //   } else if ("about".equals(key)) {
         //           showDialog(0);
         //   } else if ("help".equals(key)) {
         //       showDialog(1);
            } else if ("about_source".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SOURCE_PAGE)));
        		finish();
            } else if ("about_homepage".equals(key)) {
        		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(HOMEPAGE)));
        		finish();
            } else if ("presse".equals(key)) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.spiegel.de/spiegel/print/d-13525746.html")));
     //               finish();
            } else if ("about_market_app".equals(key)) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://details?id=%s", getPackageName()))));
                    finish();
            } else if ("about_market_publisher".equals(key))  {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:\"Markus Hoffmann\"")));
                    finish();
            }
            return false;
    }
}
