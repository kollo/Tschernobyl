package de.drhoffmannsoftware;


import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Button;

public class TBInfoActivity extends MyActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
        Button fertig = findViewById(R.id.okbutton);
        TextView readme = findViewById(R.id.description);
        readme.setText(Html.fromHtml(getResources().getString(R.string.readme)+
        		getResources().getString(R.string.news)+getResources().getString(R.string.impressum)
        		));
        fertig.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {finish();}
        });
    }
}
