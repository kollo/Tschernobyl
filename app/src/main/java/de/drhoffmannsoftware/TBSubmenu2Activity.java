package de.drhoffmannsoftware;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

public class TBSubmenu2Activity extends MyActivity {
  private static int mid;
  private String[] menutitel;
  private static String titel;

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.submenu);
    if(mid==2) menutitel=getResources().getStringArray(R.array.menutitel2);/* Belastung */
    else 
        Toast.makeText(getApplicationContext(),"ERROR: falsches Menu.  mid="+mid,
		          Toast.LENGTH_SHORT).show();
    ListView liste =findViewById(R.id.submenu);
    liste.setTextFilterEnabled(true);
    TextView ueberschrift = findViewById(R.id.ueberschrift2);
    ueberschrift.setText(titel);
    Button zurueck =findViewById(R.id.zurueck);
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, menutitel);
    liste.setAdapter(adapter);
    if(mid==2 && TBProgrammActivity.level<8) {
    //	liste.setEnabled(false);
      Toast.makeText(getApplicationContext(),getResources().getString(R.string.fragment_29),
        Toast.LENGTH_LONG).show();
    }
    liste.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(mid==2) { /*Belastung */
          if(TBProgrammActivity.level<8) 
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.fragment_29),Toast.LENGTH_LONG).show();
          else {
            TBProgrammActivity.setlevel(10+(int)id);
            Intent viewIntent = new Intent(TBSubmenu2Activity.this, TBProgrammActivity.class);
            startActivity(viewIntent);
          }
        } else 
          Toast.makeText(getApplicationContext(), ((TextView) view).getText()+" "+id+" "+mid,
        		          Toast.LENGTH_SHORT).show();
        }
      });
    zurueck.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        finish();
      }
    });
  }
  public static void setmenu(int i) {
    mid=i;
  }
  public static void settitle(String t) {
    titel=t;
  }
}
