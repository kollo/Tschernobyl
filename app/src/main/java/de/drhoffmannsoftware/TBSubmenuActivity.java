package de.drhoffmannsoftware;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

public class TBSubmenuActivity extends MyActivity {
  private static int mid;
  private String[] menutitel;
  private static String titel;

  /** Called when the activity is first created. */
//	@Override
//	public void onBackPressed() {
//		super.onBackPressed();
//		level--;
//	}
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.submenu);
     
    if(mid==1)       menutitel = getResources().getStringArray(R.array.menutitel1);/*Programm */
    else if(mid==2)  menutitel = getResources().getStringArray(R.array.menutitel3);/* Grenzwerte */
    else if(mid==3)  menutitel = getResources().getStringArray(R.array.menutitel4);
    else if(mid==4)  menutitel = getResources().getStringArray(R.array.menutitel5);
    else if(mid==10) menutitel = getResources().getStringArray(R.array.menutitel2); /* Belastung*/
    else Toast.makeText(getApplicationContext(),"ERROR: unknown menu.  mid="+mid,
    		      Toast.LENGTH_SHORT).show();

    ListView liste = findViewById(R.id.submenu);
    liste.setTextFilterEnabled(true);
    TextView ueberschrift = findViewById(R.id.ueberschrift2);
    ueberschrift.setText(titel);
    Button zurueck = findViewById(R.id.zurueck);
    
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, menutitel);
    liste.setAdapter(adapter);
    liste.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(mid==1) { /*Programm */
          if(id==0) {  /*Programm start*/
            Intent viewIntent = new Intent(TBSubmenuActivity.this, TBProgrammActivity.class);
            startActivity(viewIntent);	
          } else if(id==1) {/*Programm neustart*/
            TBProgrammActivity.setlevel(0);
            Intent viewIntent = new Intent(TBSubmenuActivity.this, TBProgrammActivity.class);
            startActivity(viewIntent);	
          } else if(id==2) {/*Belastung*/
            TBSubmenu2Activity.setmenu((int)id);
            TBSubmenu2Activity.settitle(menutitel[(int)id]);
            Intent viewIntent = new Intent(TBSubmenuActivity.this, TBSubmenu2Activity.class);
            startActivity(viewIntent);	
          } else if(id==3) {/*Ergebnisse*/			
            if(TBProgrammActivity.level<8) {
              Toast.makeText(getApplicationContext(),getResources().getString(R.string.fragment_29),Toast.LENGTH_LONG).show();
            } else {
              TBProgrammActivity.setlevel(8);
              Intent viewIntent = new Intent(TBSubmenuActivity.this, TBProgrammActivity.class);
              startActivity(viewIntent);	
            }
          } else if(id==4) {/*Erklaerungen*/
            TBPageActivity.setmenu(0);
            Intent viewIntent = new Intent(TBSubmenuActivity.this, TBPageActivity.class);
            startActivity(viewIntent);	
          } else if(id==5) {/*Literaturliste*/
            TBPageActivity.setmenu(1);
            Intent viewIntent = new Intent(TBSubmenuActivity.this, TBPageActivity.class);
            startActivity(viewIntent);	
          }
        } else if(mid==2) {/* Grenzwerte */
          String[] grenzwerte;
          grenzwerte = getResources().getStringArray(R.array.grenzwerte);
          if(id<grenzwerte.length) {
            showDialog((int) id);
//        	Toast.makeText(getApplicationContext(),Html.fromHtml(grenzwerte[(int)id]),Toast.LENGTH_LONG).show();
          }
        } else if(mid==3) {/* Nat Dosis */
          String[] natdosis;
          natdosis = getResources().getStringArray(R.array.natdosis);
          if(id<natdosis.length) {
            showDialog((int)id+10);
//            	Toast.makeText(getApplicationContext(),Html.fromHtml(natdosis[(int)id]),Toast.LENGTH_LONG).show();
          }
        } else if(mid==4) {	
          TBBeispielActivity.setmenu((int)id);
          TBBeispielActivity.settitle(menutitel[(int)id]);
          Intent myIntent = new Intent(TBSubmenuActivity.this, TBBeispielActivity.class);
          startActivity(myIntent); 
        } else 
          Toast.makeText(getApplicationContext(), ((TextView) view).getText()+" "+id+" "+mid,
        		          Toast.LENGTH_SHORT).show();
      }
    });
    zurueck.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        finish();
      }
    });	 
  }
    
    
  @Override
  protected Dialog onCreateDialog(final int id) {
    Dialog dialog = new Dialog(TBSubmenuActivity.this);
    final TextView wV = new TextView(this);  
    dialog.setContentView(wV);
    dialog.setCanceledOnTouchOutside(true);
    String t="";

    if(id<10) {
      String[] grenzwerte;
      dialog.setTitle(getResources().getString(R.string.word_grenzwert));
      grenzwerte = getResources().getStringArray(R.array.grenzwerte);
      if(id<grenzwerte.length) {
        t=grenzwerte[id]; 
      }
//     dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);              
    } else if(id<20) {
      String[] natdosis;
      natdosis=getResources().getStringArray(R.array.natdosis);
      dialog.setTitle(getResources().getString(R.string.word_strahlungsquelle));
      if(id-10<natdosis.length) {
        t=natdosis[id-10]; 
      }
    } 
    wV.setText(Html.fromHtml(t));
    return dialog;
  }
    
  public static void setmenu(int i) {
    mid=i;
  }
  public static void settitle(String t) {
    titel=t;
  }
}
