package de.drhoffmannsoftware;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.util.Date;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.WebView;
import android.widget.TextView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;
//import android.widget.ScrollView;
import android.widget.Button;
import android.content.pm.PackageManager.NameNotFoundException;


public class TBProgrammActivity extends MyActivity {
  private static final String TAG = "TSCH_BYL";
  public static int level=0;
  private static double al=-1;  /*Luftaktivitaet*/
  private static double dschmrem=-1;
  private static double dinh=-1;  /*Inhalierte Dosis*/
  private static double dinhsch=-1;  /*Inhalierte SchilddruesenDosis*/
  private static double tage=-1;  /*Tage bis Regen*/
  private static double bl=-1;  /*Bodenbelastung*/
  private static double milch=-1;  /*Belastung Nahrung*/
  private static double salat=-1;
  private static double dess,s5,dlmrem;
  private static double dbjahr=-1;
  private static double dbjahrw=-1;
  private static double dges,d50,dges650,dges65050;
  private static double faktor=-1;
  private static double auto,flug,zig,krp,gen,dfilter,d10f,rhein;
  private static double nbel=-1; /*Nahrungsbelastung */
  private static int qs=0,dys=0; /*mit niedrigeren Werten rechnen*/
  private static String Ergebnistabelle;
  private TextView btext1b;
  private TextView btext2;
  private TextView btext3;
  private Button weiterbutton,fertig,next;
  private EditText eingabe,eingabe2;
  private RadioGroup radio;

  //private ScrollView scrollview;
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.beispiel1);
    WebView webview;
    TextView btitel;
    RadioButton radio0,radio1;
    String[] text1,titel;
    weiterbutton=findViewById(R.id.weiterbutton);
    fertig=	 findViewById(R.id.fertig);
    next=	 findViewById(R.id.next);
    weiterbutton.setEnabled(false);
    eingabe=  findViewById(R.id.editText1);
    eingabe2= findViewById(R.id.editText2);
    radio=    findViewById(R.id.radioGroup1);
    radio0=   findViewById(R.id.radio0);
    radio1=   findViewById(R.id.radio1);
    btitel=   findViewById(R.id.btitel);
    webview=  findViewById(R.id.webView1);
//    scrollview=(ScrollView) findViewById(R.id.scroll);
    TextView btext1 = findViewById(R.id.btext1);
    btext1b=findViewById(R.id.btext1b);
    btext2= findViewById(R.id.btext2);
    btext3= findViewById(R.id.btext3);
    btext2.setVisibility(View.INVISIBLE);
    btext3.setVisibility(View.INVISIBLE);
    titel = getResources().getStringArray(R.array.programmtitel);
    text1 = getResources().getStringArray(R.array.programmtext1);
        

    /*Titel der Seite setzen*/
    btitel.setText(titel[level]); 

    /*Themen-Info der Seite setzen*/
    if(level==0 || level==9) btext1.setText(Html.fromHtml(text1[level]));
    else if(level==1) btext1.setText(Html.fromHtml(String.format(getResources().getString(R.string.fragment_27),displayactivity(al))+"<p/>"+text1[level]));
    else if(level==2) btext1.setText(Html.fromHtml(String.format(getResources().getString(R.string.fragment_28),displayactivity(bl))+"<p/>"+text1[level]));

    /*Vorbereitungen je nach level*/

    if(level==2 || level==5 || level==6|| level==9) {
      radio0.setText(getResources().getString(R.string.yes));
      radio1.setText(getResources().getString(R.string.no));
      if(level==2) radio.check(R.id.radio0);
      else radio.check(R.id.radio1);
      if(level==2) {
        radio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
          public void onCheckedChanged(RadioGroup group, int checkedId) {
            if(checkedId==R.id.radio1) {
              weiterbutton.setEnabled(true);
              eingabe.setEnabled(false);
            } else eingabe.setEnabled(true);
          }
        });
      } else if(level==5) {
        radio.setOnCheckedChangeListener(new OnCheckedChangeListener(){
 	  public void onCheckedChanged(RadioGroup group, int checkedId) {	
	    if(checkedId==R.id.radio1) {
	      weiterbutton.setEnabled(true);
	      eingabe2.setEnabled(false);
              btext1b.setText("");
	      qs=0;
	    } else {
	      eingabe2.setEnabled(true);
	      eingabe2.setVisibility(View.VISIBLE);
	      btext1b.setText(getResources().getString(R.string.fragenahrungbelastet));
	      qs=1;
	    }
	  }	
        });
      } else if(level==9) {
       /* TODO */
        radio.setOnCheckedChangeListener(new OnCheckedChangeListener(){
     	  public void onCheckedChanged(RadioGroup group, int checkedId) {	
    	    if(checkedId==R.id.radio1) {
    	      btext1b.setText("");
    	    } else {
    	      savedata(Ergebnistabelle);
    	      btext1b.setText(getResources().getString(R.string.saved));
/* Direkt als email verschicken .....
    	      Intent i = new Intent(Intent.ACTION_SEND);
    	      i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ "a","b" });
    	      i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ergebnisse von TSCH_BYL");
    	      i.putExtra(android.content.Intent.EXTRA_TEXT, Ergebnistabelle);
    	
    	      startActivity(Intent.createChooser(i, "Send email"));
    	*/
    	    }
    	  }	
        });
      }
    } else radio.setVisibility(View.GONE);
    if(level!=5 && level!=9) btext1b.setVisibility(View.GONE);
    if(level!=5) eingabe2.setVisibility(View.GONE);
    if(level!=0 && level!=1 && level!=2) eingabe.setVisibility(View.GONE);
    if(level!=0 && level!=1 && level!=2 &&  level!=5 && level!=6) weiterbutton.setVisibility(View.GONE);
    if(level!=8) webview.setVisibility(View.GONE);
    if(level!=3 && level!=4 && level!=7 && level!=8) next.setEnabled(false);
    if(level>=9) next.setVisibility(View.GONE);
    if(level==3) { /*personal dose */
      String t;
      double db;
      db=java.lang.Math.floor(bl/10000.0/1000.0*1000.0)/1000.0;
      dbjahr=java.lang.Math.floor(bl/1000.0*1000.0)/1000.0;
      dbjahrw=java.lang.Math.floor(dbjahr/30.0*10)/10.0;
      t=String.format(getResources().getString(R.string.fragment_30),displaydosis_flexible(db),displaydosis_flexible(dbjahr))+"<p/>";
      t=t+String.format(getResources().getString(R.string.fragment_31),displaydosis_flexible(110))+"<p/>";
      t=t+String.format(getResources().getString(R.string.fragment_32),displaydosis_flexible(dbjahrw))+"<p/>";
      if(db>600) {
        t=t+getResources().getString(R.string.umsiedelung1);
      } else if(dbjahr*30/4>250000) {
        t=t+getResources().getString(R.string.umsiedelung2);
      } else if(dbjahr*30/4>25000) {
        t=t+getResources().getString(R.string.dekontaminiert);
      } else if(bl>200000) {
        t=t+getResources().getString(R.string.zelten);
      }
      btext1.setText(Html.fromHtml(t));
    } else if(level==4) {  /* food */
      String t;
      milch=java.lang.Math.floor(bl/1000)*10;
      salat=java.lang.Math.floor(bl/300)*10;
      t=String.format(getResources().getString(R.string.fragment_33),displayactivity(milch),displayactivity(salat))+"<p/>";
      if(milch>250) t = t + getResources().getString(R.string.milchsalatverbot);
      btext1.setText(Html.fromHtml(t));
    } else if(level==5) {  /* food 2*/
      String t;
      dess=bl/500*1.5;
      s5=milch/5;
      t=String.format(getResources().getString(R.string.fragment_34),displayactivity(s5),displaydosis_flexible(dess))+"<p/>";
      if(dess>950) t=t+getResources().getString(R.string.nahrungnurgrenzwert);
      if(dess>30000) {
        t=t+getResources().getString(R.string.frageanderenahrung);
        eingabe2.setEnabled(false);
        weiterbutton.setEnabled(true);
      } else {
        radio.setVisibility(View.GONE);
        weiterbutton.setVisibility(View.GONE);
        eingabe2.setVisibility(View.GONE);
        next.setEnabled(true);
      }
      btext1b.setText("");
      btext1.setText(Html.fromHtml(t));
    } else if(level==6)  {  /*total dose */
      String t;
      if(dess>975) {
        faktor=java.lang.Math.floor((dess+dbjahr/10+dlmrem)/(975+dbjahr/10+dlmrem)*10)/10;
      }
      dges=java.lang.Math.floor(dess+dbjahr/10+dlmrem);
      d50=java.lang.Math.floor(4*dess+dbjahr/10+dlmrem);
      t=String.format(getResources().getString(R.string.fragment_35),displaydosis_flexible(dges),
        displaydosis_flexible(110),displaydosis_flexible(d50),displaydosis_flexible(5000))+"<p/>";
      if(dges>600000)      t=t+getResources().getString(R.string.tod1);
      else if(dges>200000) t=t+getResources().getString(R.string.gesundschaden1);
      if(s5>650) {
        dges650=975+dbjahr/10+dlmrem;
        dges65050=4*975+dbjahr/10+dlmrem;
        t=t+String.format(getResources().getString(R.string.fragment_36),displaydosis_flexible(dges650),
	displaydosis_flexible(dges65050))+"<p/>";
        if(dges650>600000)      t=t+getResources().getString(R.string.tod2);
        else if(dges650>200000) t=t+getResources().getString(R.string.gesundschaden2);
        t=t+getResources().getString(R.string.frageniedrigewerte);
        weiterbutton.setEnabled(true);
      } else {
        radio.setVisibility(View.GONE);
        weiterbutton.setVisibility(View.GONE);
        next.setEnabled(true);
      }
      btext1.setText(Html.fromHtml(t));
    } else if(level==7)  {
      String t;
      krp=java.lang.Math.floor(dges/10);
      double kr=dges/10/1000000*100;
      auto=java.lang.Math.floor(dges/10*100);
      flug=java.lang.Math.floor(dges*650/900)/10;
      zig=java.lang.Math.floor(dges*0.75)/10;
      double wein=java.lang.Math.floor(dges*0.5)/10;
      double alpin=java.lang.Math.floor(dges*1.5)/10;
      double lebensr=java.lang.Math.floor(dges*0.3)/10;
      gen=java.lang.Math.floor(dges/10*0.2);
      t=String.format(getResources().getString(R.string.fragment_43),displaymenge(kr),displaymenge(krp))+"<p/>"+
       	"&nbsp;"+(int)roundSignificant(auto,2)+" "+getResources().getString(R.string.fragment_44)+"<br/>"+
       	"&nbsp;&nbsp;"+roundSignificant(flug,2)+" "+getResources().getString(R.string.fragment_45)+"<br/>"+
       	"&nbsp;&nbsp;&nbsp;"+roundSignificant(zig,2)+" "+getResources().getString(R.string.fragment_46)+"<br/>"+
       	"&nbsp;&nbsp;&nbsp;&nbsp;"+roundSignificant(wein,2)+" "+getResources().getString(R.string.fragment_47)+"<br/>"+
       	"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+roundSignificant(alpin,2)+" "+getResources().getString(R.string.fragment_48)+"<br/>";
      if(lebensr>0) {
       	t=t+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+roundSignificant(lebensr,2)+" "+getResources().getString(R.string.fragment_49)+"<br/>";
      }
      t=t+"<p/>"+String.format(getResources().getString(R.string.fragment_50),displaymenge(gen))+"<p/>";
      if(dess<=975 || dys>0) {
        t=t+getResources().getString(R.string.fragment_51)+"<p/>";
      }
      if(dys==0 && dess>975) {
       	t=t+String.format(getResources().getString(R.string.fragment_52),displaymenge(faktor))+"<p/>";
      }
      t=t+getResources().getString(R.string.fragment_53);
      btext1.setText(Html.fromHtml(t));
    } else if(level==8)  {
      String t;
      double s5a=s5;
      double s50a=s5a/10;
      dfilter=java.lang.Math.floor(al*tage/60*1000)/1000;
      d10f=java.lang.Math.floor(dfilter/10*1000)/1000;
      double d103=java.lang.Math.floor(d10f/3*1000)/1000;
      double df3=java.lang.Math.floor(dfilter/3*1000)/1000;
       		
      t="<b>"+getResources().getString(R.string.word_luftaktivitaet)+"</b> "+al+" Bq/m³ "+
        String.format(getResources().getString(R.string.word_fuertage),displaymenge(tage))+"<b><br/>"+
	getResources().getString(R.string.word_bodenaktivitaet)+"</b> "+bl+" Bq/m²<br/>";
      if(dys>0) {
       	t=t+getResources().getString(R.string.fragment_67)+"<br/>";
      } else if (qs>0) {
       	t=t+String.format(getResources().getString(R.string.fragment_68),displayactivity(nbel))+"<br/>";
      }
      t=t+"<table border=1 cellspacing=0><tr><td><b>"+
	getResources().getString(R.string.word_belastung)+"</b></td><td><b>"+
	getResources().getString(R.string.word_maximalwerte)+"</b></td></tr>";
      t=t+"<tr><td>"+
        getResources().getString(R.string.word_koerperdosisluftaktivitaet)+" </td><td>"+
	displaydosis_flexible(dlmrem)+"</td></tr>";
      t=t+"<tr><td>"+
        getResources().getString(R.string.word_schilddruesendosiskleinkindinhalation)+"</td><td>"+
	displaydosis_flexible(dschmrem)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_vergleich_radon)+"</td></tr>"+
          "<tr><td>"+getString(R.string.word_im_wohnzimmer)+"</td><td>"+displaydosis_flexible(120)+"/"+"Jahr"+"</td></tr>"+
          "<tr><td>"+getString(R.string.word_im_freien)+"</td><td>"+displaydosis_flexible(45)+"/"+"Jahr"+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_koerperdosis_folgejahr)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_wiese_liegen)+"</td><td>"+displaydosis_flexible(dbjahr)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_normale_lebensweise)+"</td><td>"+displaydosis_flexible(dbjahrw)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_natuerliche_dosis)+"</td><td>"+displaydosis_flexible(110)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_jodkontamination)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_milch)+"</td><td>"+displayactivity(milch)+"/l</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_salat)+"</td><td>"+displayactivity(salat)+"/kg</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_caesium_kontamination)+"</td><td>"+displayactivity(s5a)+"/kg</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_strontium_kontamination)+"</td><td>"+displayactivity(s50a)+"/kg</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_vergleich_kalium)+"</td><td>"+getString(R.string.word_kalium_wert)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_jod_kata)+"</td><td>"+getString(R.string.word_jod_wert)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_jod_alle)+"</td><td>"+getString(R.string.word_jod_alle_wert)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_zusatzdosis_nahrung_folgejahr)+"</td><td>"+displaydosis_flexible(dess)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_vergleich_kalium2)+"</td><td>"+displaydosis_flexible(20)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_gesamtdosis_folgejahr)+"</td><td>"+displaydosis_flexible(dges)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_dosis_50jahre)+"</td><td>"+displaydosis_flexible(d50)+"</td></tr>";
      t=t+"</table><p/>";
      t=t+"<b>"+getString(R.string.word_aequivalentes_risiko)+"</b><br/>";
      t=t+"<table border=1 cellspacing=0><tr><td><b>"+getString(R.string.word_vergleich)+"</b></td><td><b>"+getResources().getString(R.string.word_maximalwerte)+"</b></td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_autofahrt)+"</td><td>"+(int)roundSignificant(auto,2)+" km ("+getString(R.string.word_im_jahr)+")"+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_flugstunden)+"</td><td>"+roundSignificant(flug,2)+" "+getString(R.string.word_std)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_zigaretten)+"</td><td>"+roundSignificant(zig,2)+" "+getString(R.string.word_stueck)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_zusaetzliche_krebserkrankungen)+"</td><td>"+roundSignificant(krp,2)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_vergleich_krebs)+"</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_genetisches_risiko)+"</td><td>"+roundSignificant(gen,2)+"</td></tr>";
      t=t+"</table><p/>";
      t=t+"<b>"+getString(R.string.word_weitere_sonderfaelle)+"</b><br/>";
      t=t+"<table border=1 cellspacing=0><tr><td><b>"+getResources().getString(R.string.word_belastung)+"</b></td><td><b>"+getResources().getString(R.string.word_maximalwerte)+"</b></td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_oberflaechendosisleistung_luftfilter)+"</td><td>"+displaydosis_nounit(d10f)+" "+getString(R.string.word_bis)+" "+displaydosis(dfilter)+"/h</td></tr>";
      t=t+"<tr><td>"+getString(R.string.word_nach_8_wochen)+"</td><td>"+displaydosis_nounit(d103)+" "+getString(R.string.word_bis)+" "+displaydosis(df3)+"/h</td></tr>";
      t=t+"</table>";
      t=t.replace("µ", "&micro;");
      t=t.replace("²", "&sup2;");
      t=t.replace("³", "&sup3;");
      Ergebnistabelle=t;
      webview.loadDataWithBaseURL(null, t, "text/html", "utf-8", null);
      btext1.setText("");
    } else if(level==9)  {
      btext1b.setText(getResources().getString(R.string.fragment_37));
    } else if(level==10)  {
      String t;
      t=String.format(getResources().getString(R.string.fragment_95),displaymenge(tage),displaydosis(d10f),displaydosis(dfilter))+"<p/>"+
      getResources().getString(R.string.fragment_76)+"<p/>";
      if(dfilter<0.05) {
        t=t+getResources().getString(R.string.fragment_69);
      } else {
      	if(dfilter>100) {
      	  t=t+getResources().getString(R.string.fragment_70);
      	} else if(dfilter>=10) {
      	  t=t+getResources().getString(R.string.fragment_71);
	}
      	t=t+"<p/>"+getResources().getString(R.string.fragment_72)+"<p/>";
      	if(dfilter>1) t=t+getResources().getString(R.string.fragment_73);
      }		
      btext1.setText(Html.fromHtml(t));
    } else if(level==11) {
      String t;
      double fisch=rhein/10*2;
      if(bl<10000) {
      	t="<font color=#00ff00><b>"+getResources().getString(R.string.fragment_74)+"</b></font>";
      } else if(bl<200000) {
      	t=getResources().getString(R.string.fragment_93)+
	  String.format(getResources().getString(R.string.fragment_94),displaydosis(fisch));
      } else {
      	t=getResources().getString(R.string.fragment_75);
      }
      btext1.setText(Html.fromHtml(t));
    } else if(level==12)  {
      String t;
      double schwimmen=0.004*rhein/10;
      if(bl<10000) { // keine Gefahr
      	t="<font color=#00ff00><b>"+getResources().getString(R.string.fragment_90)+"</b></font>";
      } else if(bl<200000) {
      	t=getResources().getString(R.string.fragment_91)+"<p/>"+
	  String.format(getResources().getString(R.string.fragment_92),displaydosis(schwimmen))+
	  "<p/>"+getResources().getString(R.string.fragment_89);
      } else {
        t="<b>"+getResources().getString(R.string.fragment_88)+"</b><p/>";
      }
      btext1.setText(Html.fromHtml(t));
    } else if(level==13)  {
      String t;
      if(rhein<100) {
      	t="<font color=#00ff00><b>"+getResources().getString(R.string.fragment_87)+"</b></font><p/>";
      } else if(rhein>1000) {
      	t=getResources().getString(R.string.fragment_86);
      } else {
      	t=getResources().getString(R.string.fragment_85);
      }
      btext1.setText(Html.fromHtml(t));
    } 
    eingabe.addTextChangedListener(new TextWatcher(){
      public void onTextChanged(CharSequence s, int start, int before, int count) {}
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
      public void afterTextChanged(Editable s) {
        weiterbutton.setEnabled(s.length()>0);
      }
    });
    weiterbutton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        weiterbutton.setEnabled(false);
        eingabe.setEnabled(false);
        eingabe2.setEnabled(false);
        if(level==0) {
          String t;
          al=Double.parseDouble(eingabe.getText().toString());
          btext2.setText("");
          if(al<0.5) {
            t=getResources().getString(R.string.fragment_83);
          } else if(al>10000000) {
            t=getResources().getString(R.string.fragment_84);
          } else {
            double ddir=java.lang.Math.floor(2*al/1000.0*1000.0)/1000.0;
            dinh=java.lang.Math.floor(al/100.0*1000.0)/1000.0;
            dinhsch=java.lang.Math.floor(al*1.7/24*1000.0)/1000.0;
            t=String.format(getResources().getString(R.string.fragment_82),
	      displaydosis(ddir),displaydosis(dinh),displaydosis(dinhsch))+"<p/>";
            if(dinh>0.01) {
              t=t+"<font color=#ffff00><b>"+getResources().getString(R.string.fragment_80)+
	        "</b><p/><b>"+getResources().getString(R.string.fragment_81)+"</b></font><p/>";
              if(dinh>10000) {
                t=t+"<font color=#ff0000><b>"+getResources().getString(R.string.fragment_79)+"</b></font><p/>";
              } else if(dinh>150) {
       	        double tkrit1=java.lang.Math.round(30000/dinh/24*10)/10.0;
                double tkrit2=java.lang.Math.round(300000/dinh/24*10)/10.0;
                t=t+String.format(getResources().getString(R.string.fragment_78),displaymenge(tkrit1),displaymenge(tkrit2))+"<p/>"+
                  "<b>"+getResources().getString(R.string.fragment_77)+"</b><p/>";
              } else if(dinh>0.1) {
                t=t+getResources().getString(R.string.fragment_66)+"<p/>";
              }
            } 
            next.setEnabled(true);
          }
          btext3.setText(Html.fromHtml(t));
        } else if(level==1) {
          String t;
          double hoe;
          tage=Double.parseDouble(eingabe.getText().toString());
          btext2.setText("");
          dlmrem=java.lang.Math.floor(dinh*tage*24.0*1000)/1000.0;
          dschmrem=java.lang.Math.floor(tage*24.0*dinhsch*1000)/1000.0;
          hoe=java.lang.Math.floor(tage*110.0/365*1000)/1000.0;
          t=String.format(getResources().getString(R.string.fragment_64),displaydosis_flexible(dlmrem))+"<p/>"+
            String.format(getResources().getString(R.string.fragment_65),displaydosis_flexible(dschmrem))+"<p/>";
          if(dlmrem>1000000) {
            t=t+"<font color=#ff0000><b>"+getResources().getString(R.string.fragment_61)+"</b></font> "+
              getResources().getString(R.string.fragment_62)+"<p/><b>"+getResources().getString(R.string.fragment_63)+
	      "</b><p/>";
          } else { 
            t=t+String.format(getResources().getString(R.string.fragment_60),displaydosis(hoe))+"<p/>";
            if(dschmrem>30000) {
              t=t+"<font color=#ffff00><b>***** "+getResources().getString(R.string.fragment_59)+" *****</b></font><br/>";
            }
            if(dschmrem>100000) {
              t=t+getResources().getString(R.string.fragment_58)+"<p/>";
            }
            if(dschmrem<10000) {
              t=t+String.format(getResources().getString(R.string.fragment_57),displaydosis(100000))+"<p/>";
            }
            bl=java.lang.Math.floor(al*100.0)*10.0;
            next.setEnabled(true); 
          }
          btext3.setText(Html.fromHtml(t));
        } else if(level==2) {
          String t;
          double gmz;
          btext2.setText("");
          if(radio.getCheckedRadioButtonId()==R.id.radio0) {
            bl=Double.parseDouble(eingabe.getText().toString());
            btext2.setText(String.format(getResources().getString(R.string.fragment_54),displayactivity(bl)));
          }
          radio.setEnabled(false);
          rhein=java.lang.Math.floor(bl/1000.0);
          gmz=java.lang.Math.floor((bl/40000.0+1.0)*10.0)/10.0;
          t=String.format(getResources().getString(R.string.fragment_55),displayactivity(rhein))+"<p/>";
          if(gmz>1.09) {
            t=t+String.format(getResources().getString(R.string.fragment_56),displaymenge(gmz))+"<p/>";
          }
          next.setEnabled(true); 
          btext3.setText(Html.fromHtml(t));
        } else if(level==5) {
          String t=getResources().getString(R.string.fragment_42);
          radio.setEnabled(false);
          if(radio.getCheckedRadioButtonId()==R.id.radio0) {
            nbel=Double.parseDouble(eingabe2.getText().toString());
            dess=nbel/100*150;
            s5=nbel;
            btext2.setText(String.format(getResources().getString(R.string.fragment_41),displaydosis(dess)));
          } else btext2.setText(getResources().getString(R.string.fragment_40));
          next.setEnabled(true); 
          btext3.setText(Html.fromHtml(t));
        } else if(level==6) {
          String t=getResources().getString(R.string.fragment_39);
          radio.setEnabled(false);
          if(radio.getCheckedRadioButtonId()==R.id.radio0) {
            dges=dges650;
            d50=dges65050;
            dess=975;
            dys=1;
            btext2.setText(getResources().getString(R.string.fragment_38));
          } else {
            dys=0;
            btext2.setText("");
          }
          next.setEnabled(true); 
          btext3.setText(Html.fromHtml(t));
        } else btext2.setText("Test: "+level+" ");
        btext2.setVisibility(View.VISIBLE);
        btext3.setVisibility(View.VISIBLE);
        fertig.setVisibility(View.VISIBLE);
      }
    });
    fertig.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        finish();
      }
    });	
    next.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        level++;
        Intent viewIntent = new Intent(TBProgrammActivity.this, TBProgrammActivity.class);
        startActivity(viewIntent);	
        finish();
      }
    });
  }
  public static void setlevel(int i) {
    level=i;
  }
	
  private void savedata(String content) {
    FileOutputStream fos=null;
    OutputStreamWriter osw=null;
    String filename="TSCH_BYL-out-"+(int)al+"-"+(int)tage+".html";
    /* Hier jetzt ein externe von aussen lesbares verzeichnis*/
    File dirdata=Environment.getExternalStorageDirectory();
    File dir=new File (dirdata, "Android/TSCH_BYL");
    try {
      dir.mkdirs();	
      File file=new File(dir,filename);
      fos=new FileOutputStream(file);
      osw=new OutputStreamWriter(fos);

      osw.write(getResources().getString(R.string.file_header));    	   		
      osw.write(content);
      osw.write(String.format(getResources().getString(R.string.file_footer),applicationVersion(),
        DateFormat.getDateInstance().format(new Date())));
    } catch (Throwable t) {
      // FilenotFound oder IOException
      Log.e(TAG,"open/save. ",t);
      Toast.makeText(getApplicationContext(), 
        String.format(getResources().getString(R.string.file_save_error),dir.toString()+"/"+filename), 
          Toast.LENGTH_LONG).show();
    } finally {
      if(osw!=null) {
    	try {
    	  osw.close();
    	} catch (IOException e) {
    	  Log.e(TAG,"osw.close ",e);
    	}
      }
      if(fos!=null) {
    	try {
    	  fos.close();
    	} catch (IOException e) {
    	  Log.e(TAG,"fos.close ",e);
    	}
      }
      Toast.makeText(getApplicationContext(), 
        String.format(getResources().getString(R.string.file_saved),dir.toString()+"/"+filename), 
	  Toast.LENGTH_LONG).show();
    }
  }
  private String applicationVersion() {
    try {
      return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
    }
    catch (NameNotFoundException x)  {
      return "unknown";
    }
  }
}
