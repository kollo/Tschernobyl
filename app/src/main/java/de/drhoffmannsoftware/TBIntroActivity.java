package de.drhoffmannsoftware;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TBIntroActivity extends Activity {
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Button fertig,anleitung;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);
        fertig=findViewById(R.id.okbutton);
        anleitung=findViewById(R.id.instructions);
        fertig.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		finish();
        	}
        });
        anleitung.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		TBPageActivity.setmenu(0);
        		Intent viewIntent = new Intent(TBIntroActivity.this, TBPageActivity.class);
        		startActivity(viewIntent);
        	}
        });
    }
}
