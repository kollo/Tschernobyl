Guide to contributing to Tschernobyl for Android
=================================================

The App used to be a paid app in the Google Play Store, and has been there since
2011. I thought, the accident in Fukushima was a good reason to republish the
program which evolved from a version on the ATARI ST 1987 after the Tschernobyl
accident.  Because the radioactive cloud reached germany, the interest in this
app was pretty high.

The app whould have been most useful for users in Japan. At least an english
version would have helped a lot. But I had no time to translate it at that time. 
I have stopped development on this app myself, because I have not enough time to
maintain it. Also I do not expect the next nuclear power accident coming soon.
But, if someone wants to further contribute I suggest following improvements:

1. Translate the included help/documentation and the main texts into english and japanese, maybe even into more languages.

2. Add more up to date references to the literature list.

3. Adapt the layout a little bit to more recent Android devices.

best regards
Markus Hoffmann, December 2016


## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
licence GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 

## Contributing with a Merge or Pull Request

The best way to contribute to this project is by making a merge/pull request:

1. Login with your codeberg account or create one now.
2. [Fork](https://codeberg.org/kollo/Tschernobyl.git) the Tschernobyl app for Android repository. 
Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the 
   `contrib` directory if you're not sure where your contribution might fit.
5. Edit `ACKNOWLEGEMENTS` and add your own name to the list of contributors 
   under the section with the current year. Use your name, or a 
   codeberg/github ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a merge/pull request against the Tschernobyl repository.


## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a merge/pull request, then you can file an Issue. Filing an Issue will 
help us see the problem and fix it.

Create a [new Issue](https://codeberg.org/kollo/Tschernobyl/issues/new?issue) now!

## Thanks

We are very grateful for your support.


